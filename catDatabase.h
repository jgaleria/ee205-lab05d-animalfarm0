/////////////////////////////////////////
//
// Author : Joshua Galeria
// Date   : 2/18/2022
// Name   : catDatabase
// Desc   : Defines struct Cat 
//
// ///////////////////////////////////

#ifndef CATDATABASE_H
#define CATDATABASE_H

//Header
#include <stdio.h>
#include <stdbool.h>

// Define max numbers
#define MAX_NAME (32) 
#define MAX_CATS (100)

// enums Gender and Breed
enum Gender {UNKNOWN_GENDER, MALE, FEMALE} gender;
enum Breed  {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

// Array of structs
struct Cat
{
   //Name
   char         name[MAX_NAME];
   //Gender
   enum  Gender gender; 
   //Breed
   enum  Breed  breed;
   //isFixed
   bool         isFixed;
   //Weight
   float        weight;
};

#endif
