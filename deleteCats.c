///////////////////////////////
//
// Author : Joshua Galeria
// Date   : 2/18/2022
// Name   : deleteCats.c
// Desc   : Delete for cats
//
//////////////////////////////

//Headers
#include "catDatabase.h"
#include <stdio.h>
#include <string.h>

//Global Variables
extern struct Cat Database[MAX_CATS];
extern int    numberOfCats;

//Delete all cats
void deleteAllCats() {
   for( int x = 0; x < numberOfCats; x++ ) {
      strcpy(Database[x].name, "");
      Database[x].gender  = 0;
      Database[x].breed   = 0;
      Database[x].isFixed = false;
      Database[x].weight  = 0.0;
   } 
} 

//Delete specific cat
void deleteCat( int index ) {
   strcpy(Database[index].name, "");
   Database[index].gender  = 0;
   Database[index].breed   = 0;
   Database[index].isFixed = false;
   Database[index].weight  = 0.0;

}
