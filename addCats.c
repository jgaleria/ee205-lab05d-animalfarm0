///////////////////////////////////
//
// Author : Joshua Galeria 
// Date   : 2/18/2022
// Name   : addCats.c
// Desc   : Add cats to database 
//
//////////////////////////////////

#include "catDatabase.h"
#include <stdio.h>
#include <string.h>

extern struct Cat Database[MAX_CATS];
extern int numberOfCats;

int addCat ( char name[MAX_NAME], enum Gender gender, enum Breed breed, bool isFixed, float weight ) {
   
   //Error Checks:
   if ( numberOfCats > MAX_CATS ) { 
      printf("You have exceeded the max number of cats\n");
      return 0;
   } 
   if ( strlen(name) == 0 ) { 
      printf("Your name must not be empty\n");
      return 0;
   } 
   if ( strlen(name) > MAX_NAME ) {
      printf("Your name must be less than or equal to [%d] characters\n", MAX_NAME);
      return 0;
   } 
   if ( strlen(name) > 0 ) {
      for(int x = 0; x < MAX_CATS; x++ ) { 
         if( strcmp(Database[x].name, name) == 0 ) {
            printf("Your cat must have a unique name\n");
            return 0;
         }
      }
   } 
   if ( weight <= 0 ) {
      printf("The weight needs to be greater than 0\n");
   } 

   //Insert
   for(int x = 0; x < MAX_CATS; x++) {
      //Check if the slot is empty
      if( *Database[x].name == (char) 0) {
         strcpy(Database[x].name, name);
         Database[x].gender  = gender;
         Database[x].breed   = breed;
         Database[x].isFixed = isFixed;
         Database[x].weight  = weight;
         
         //Add to number of cats
         numberOfCats += 1;
         printf("Added a new cat!\n");

         return x;
      }

   } 
   return 0;
} 
