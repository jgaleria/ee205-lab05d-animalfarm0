///////////////////////
//
// Author : Joshua Galeria 
// Date   : 2/18/2022
// Name   : reportCats.c
// Desc   : View for database
//
//////////////////////

#include "catDatabase.h"
#include <stdio.h>
#include <string.h>

extern struct Cat Database[MAX_CATS];
extern int    numberOfCats;

//Print cat at location in database
void  printCat( int index ) { 

   //Error check: 
   if ( index < 0 || index > MAX_CATS ) {
      printf("animalFarm0: Bad cat [%d]\n", index);
   }   

   //View cat
   printf("You have found the cat:\n");
   printf("cat index = [%d]\nname    = [%s]\ngender  = [%d]\n", index, Database[index].name, Database[index].gender);
   printf("breed   = [%d]\nisFixed = [%d]\nweight  = [%f]\n\n", Database[index].breed, Database[index].isFixed, Database[index].weight);
   

} 


//Print name of all of the cats in database
void printAllCats() {
   printf("These are all the cats:\n");

   for ( int x = 0; x < numberOfCats; x++ ) {
      printf("[%s]\n", Database[x].name);
   }
   printf("\n");
} 

// Print index of catname 
int findCat( char name[MAX_NAME] ) { 
   for( int x = 0; x < numberOfCats; x++ ) {
      if(strcmp (Database[x].name, name) == 0) {
         printf("The index of [%s] is [%d]\n\n", Database[x].name, *Database[x].name);
         return x;
      } 
   } 
   printf("\n");
   return 0;

} 
