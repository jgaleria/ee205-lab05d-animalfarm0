//////////////////////////////
// 
// Author : Joshua Galeria
// Date   : 2/18/2022
// Name   : main.c
// Desc   : Runs entire program
//
/////////////////////////////

//Header
#include <stdio.h>
#include "catDatabase.h" 
#include "addCats.c"
#include "deleteCats.c"
#include "reportCats.c"
#include "updateCats.c"

//Declaring Global Variables
struct Cat Database[MAX_CATS];
int    numberOfCats;

int main() {
   //Starting
   printf("Starting Animal Farm 0\n\n");
   
   //Insert
   addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;
   addCat( "Milo", MALE, MANX, true, 7.0 ) ;
   addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
   addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
   addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;
   addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;

   //View
   printf("\n");
   printAllCats();

   //Update
   int kali = findCat( "Kali" ) ;
   updateCatName( kali, "Chili" ) ; // this should fail
   printCat( kali );
   updateCatName( kali, "Capulet" ) ;
   updateCatWeight( kali, 9.9 ) ;
   fixCat( kali ) ;
   printCat( kali );

   //View
   printAllCats();

   //Delete
   printf("Deleting all cats\n");
   deleteAllCats();
   printAllCats();

   //Ending
   printf("Ending Animal Farm 0\n");

}




