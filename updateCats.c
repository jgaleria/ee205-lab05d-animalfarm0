//////////////////////////
//
// Author : Joshua Galeria
// Date   : 2/18/2022
// Name   : updateCats.c
// Desc   : Update for cats
//
/////////////////////////

#include "catDatabase.h"
#include <stdio.h>
#include <string.h>

extern struct Cat Database[MAX_CATS];
extern int    numberOfCats;


int updateCatName( int index, char newName[MAX_NAME] ) {
   
   //Error check
   //Check if name is empty
   if(strlen(newName) == 0) {
      printf("The new cat name can't be empty\n");
      return 0;
   }
   //Check for duplicate names
   for (int x = 0; x < numberOfCats; x++) {
      if( x < 0){
         printf("There's already a cat with that name!\n");
         return 0;
      }
   } 
   //Update the cat name
   strcpy(Database[index].name, newName);
   return 1;
} 

void fixCat( int index ) {
   Database[index].isFixed = true;
} 

void updateCatWeight( int index, float newWeight ) {
   
   //Error check 
   if ( newWeight <= 0 ) {
      printf("The new weight needs to be greater than 0\n");
   } 
   
   //Update cat weight
   Database[index].weight = newWeight;
   
} 
