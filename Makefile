############################
#
# Author: Joshua Galeria
# Date  : 2/19/2022
# Name  : Makefile
# Desc  : Makefile for program
#
###########################

## Prereq
CC = gcc
CFLAGS = -g -Wall -Wextra
TARGET = main
all: $(TARGET)
 
## Create .o from depend
addCats.o: addCats.c
	$(CC) $(CFLAGS) -c addCats.c

updateCats.o: updataCats.c 
	$(CC) $(CFLAGS) -c updateCats.c

reportCats.o: reportCats.c 
	$(CC) $(CFLAGS) -c reportCats.c

deleteCats.o: deleteCats.c 
	$(CC) $(CFLAGS) -c deleteCats.c

catDatabase.o: catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.h
  
## Main
main.o: main.c addCats.c updateCats.c reportCats.c deleteCats.c catDatabase.h
	$(CC) $(CFLAGS) -c main.c
  
##main.o: main.c addCats.h updateCats.h reportCats.o deleteCats.o catDatabase.o
##	$(CC) $(CFLAGS) -o $(TARGET) main.c addCats.h updateCats.h reportCats.o deleteCats.o catDatabase.o
  
clean:
	rm -f $(TARGET) *.o
